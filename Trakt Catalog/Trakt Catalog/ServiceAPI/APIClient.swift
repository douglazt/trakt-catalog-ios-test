//
//  ApiClient.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import Foundation
import Alamofire

class APIClient: ServiceAPI {
    
    struct Endpoints {
        static let popularMovies:String = "/movies/popular"
        static let movieId: String = "/movies"
        static let queryForMovie: String = "/search/movie"
        static let query: String = "/search/" // Add search types separated by commas
    }
    
    var clientID: String = "6d9521070b6e41f8eeb83308d13308d31d1755a8a3b173b2cb8c91ea8af00e4c" // trakt-api
    
    var APIVersion: String = "2"
    var baseURL: String = "https://api.trakt.tv"
    var headers: [String : String] {
        return ["Content-Type":"application/json",
                "trakt-api-version": APIVersion,
                "trakt-api-key": clientID]
    }
    
    // MARK: - ServiceAPI
    
    func parseResponseData<T>(responseData data: Data, forResultType type: T.Type) -> T? where T : Decodable, T : Encodable {
        
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        do {
            let json = try decoder.decode(type, from: data)
            return json
        } catch {
            return nil
        }
    }
    
    
    func getPopularMovies(atPage page: Int, completion: @escaping ServiceAPI.popularMoviesResponse) {
    
        let url = URL(string: "\(baseURL)\(Endpoints.popularMovies)")!
        let params:Parameters = ["page": page]
        
        Alamofire.request(url, method: .get, parameters: params, headers: headers).responseData { [weak self] (responseData) in
            guard let strongSelf = self else { return }
            
            if responseData.error != nil {
                completion(nil, responseData.error)
                return
            }
            
            if let data = responseData.value {
                if let movies = strongSelf.parseResponseData(responseData: data, forResultType: [Movie].self) {
                   completion(movies, nil)
                    return
                } else {
                    let err = NSError(domain: "TraktAPI", code: 999, userInfo: ["message": "Parse Error"])
                    completion(nil, err)
                    return
                }
            } else {
                let err = NSError(domain: "TraktAPI", code: 999, userInfo: ["message": "No data received"])
                completion(nil, err)
                return
            }
            
        }

    }
    
    func getMovie(byMovieID movie: Movie, completion: @escaping ServiceAPI.movieDetailsResponse) {
        
        let url = URL(string: "\(baseURL)\(Endpoints.movieId)/\(movie.ids.slug)")!
        let params:Parameters = ["extended": "full"]
        
        Alamofire.request(url, method: .get, parameters: params, headers: headers).responseData { [weak self] (responseData) in
            guard let strongSelf = self else { return }
            
            if responseData.error != nil {
                completion(nil, responseData.error)
                return
            }
            
            if let data = responseData.value {
                if let completeMovie = strongSelf.parseResponseData(responseData: data, forResultType: CompleteMovie.self) {
                    completion(completeMovie, nil)
                    return
                } else {
                    let err = NSError(domain: "TraktAPI", code: 999, userInfo: ["message": "Parse Error"])
                    completion(nil, err)
                    return
                }
            } else {
                let err = NSError(domain: "TraktAPI", code: 999, userInfo: ["message": "No data received"])
                completion(nil, err)
                return
            }
            
        }

    }
    
    func getMoviesBySearchText(searchText text: String, completion: @escaping ServiceAPI.moviesSearchResponse) {
        
        let url = URL(string: "\(baseURL)\(Endpoints.queryForMovie)")!
        let params: Parameters = ["query":text]
        
        Alamofire.request(url, method: .get, parameters: params, headers: headers).responseData { [weak self] (responseData) in
            guard let strongSelf = self else { return }
            
            if responseData.error != nil {
                completion(nil, responseData.error)
                return
            }
            
            if let data = responseData.value {
                if let searchResult = strongSelf.parseResponseData(responseData: data, forResultType: [MovieSearchResult].self) {
                    completion(searchResult, nil)
                    return
                } else {
                    let err = NSError(domain: "TraktAPI", code: 999, userInfo: ["message": "Parse Error"])
                    completion(nil, err)
                    return
                }
            } else {
                let err = NSError(domain: "TraktAPI", code: 999, userInfo: ["message": "No data received"])
                completion(nil, err)
                return
            }
            
        }
        
    }
    
}
