//
//  ServiceAPI.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import Foundation

protocol Service {
    func parseResponseData<T: Codable>(responseData data: Data, forResultType type: T.Type) -> T?
}

protocol ServiceAPI:Service {
    
    typealias popularMoviesResponse = (_ movies: [Movie]?, _ error: Error?) -> Void
    typealias movieDetailsResponse = (_ movie: CompleteMovie?, _ error: Error?) -> Void
    typealias moviesSearchResponse = (_ movieSearchResults: [MovieSearchResult]?, _ error: Error?) -> Void
    
    var clientID: String { get set }
    
    var APIVersion: String { get set }
    var baseURL: String { get set }
    var headers: [String: String] { get }
    
    func getPopularMovies(atPage page: Int, completion: @escaping popularMoviesResponse)
    func getMovie(byMovieID movie: Movie, completion: @escaping movieDetailsResponse)
    func getMoviesBySearchText(searchText text: String, completion: @escaping moviesSearchResponse)
    
}
