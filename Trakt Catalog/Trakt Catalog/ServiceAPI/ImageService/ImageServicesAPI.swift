//
//  ImageServicesAPI.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

protocol ImageServiceAPI:Service {
    
    typealias ImagesResponse = (_ response: MovieImages?, _ error: Error?) -> Void
    typealias ImageDownloadCompletion = (_ responseData: UIImage?, _ error: Error?) -> Void
    
    
    var apiKey: String { get set }
    var baseURL: String { get set }
    var headers: [String: String] { get }
    
    func getImagesForMovie(movie: Movie, completion: @escaping ImagesResponse)
    
    static func downloadImage(fromURL url: String, completion: @escaping ImageDownloadCompletion)
    static func downloadImagePoster(forMovie movie: Movie, completion: @escaping ImageDownloadCompletion)
    
}
