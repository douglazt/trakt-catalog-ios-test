//
//  ImageServicesApiClient.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class ImageServicesAPIClient: ImageServiceAPI {
    
    struct Endpoints {
        static let movies: String = "/movies"
    }
    
    var apiKey:String = "b7b6048ce16ea1c31d6b10f241540d5c" // Fanart-api
    var baseURL: String = "http://webservice.fanart.tv/v3"
    
    var headers: [String : String] {
        return ["api-key": apiKey]
    }
    
    // MARK: - API
    
    func parseResponseData<T>(responseData data: Data, forResultType type: T.Type) -> T? where T : Decodable, T : Encodable {
        let decoder = JSONDecoder()
        do {
            let json = try decoder.decode(type, from: data)
            return json
        } catch {
            return nil
        }
    }
    
    func getImagesForMovie(movie: Movie, completion: @escaping ImageServiceAPI.ImagesResponse) {
        
        //get the movieID - (imdb or tmdb)
        let id = movie.ids.imdb ?? "\(movie.ids.tmdb)"
        if id.isEmpty { return }
        let url = URL(string: "\(baseURL)\(Endpoints.movies)/\(id)")!
        
        Alamofire.request(url, method: .get, headers: headers).responseData { [weak self] (responseData) in
            guard let strongSelf = self else { return }
            
            if responseData.error != nil {
                completion(nil, responseData.error)
                return
            }
            
            if let data = responseData.value {
                if let moviesImages = strongSelf.parseResponseData(responseData: data, forResultType: MovieImages.self) {
                    completion(moviesImages, nil)
                    return
                } else {
                    let err = NSError(domain: "TraktAPI", code: 999, userInfo: ["message": "Parse Error"])
                    completion(nil, err)
                    return
                }
            } else {
                let err = NSError(domain: "TraktAPI", code: 999, userInfo: ["message": "No data received"])
                completion(nil, err)
                return
            }
            
        }
        
    }
    
    static func downloadImage(fromURL url: String, completion: @escaping ImageServiceAPI.ImageDownloadCompletion) {
        
        guard let imageURL = URL(string: url) else {
            let err = NSError(domain: "FanartAPI", code: 998, userInfo: ["message": "Invalid URL"])
            completion(nil, err)
            return
        }
        
        Alamofire.request(imageURL, method: .get).responseImage { (image) in
            completion(image.value, nil)
        }
    }
 
    
    static func downloadImagePoster(forMovie movie: Movie, completion: @escaping ImageServiceAPI.ImageDownloadCompletion) {
     
        //get the movieID - (imdb or tmdb)
        guard let imdbId = movie.ids.imdb else { return }
        
        let omdbApiKey: String = "23ed29df"
        let url = URL(string:"http://img.omdbapi.com/")!
        
        Alamofire.request(url, method: .get, parameters: ["apikey": omdbApiKey, "i": imdbId]).responseImage { (image) in
            completion(image.value, nil)
        }
        
    }
    
}





















