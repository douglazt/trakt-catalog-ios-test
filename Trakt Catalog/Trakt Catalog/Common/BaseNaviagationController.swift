//
//  BaseNaviagationController.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationBar.barStyle = .black
        navigationBar.barTintColor = .black
        navigationBar.tintColor = .white
        
    }
    
}
