//
//  Storyboards.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

struct StoryboardViewControllers {
    
    struct app {
        
        static private let storyboard = UIStoryboard(name: "App", bundle: nil)
        
        static func tabVC() -> AppTabBarController {
            return storyboard.instantiateViewController(withIdentifier: "AppTabBarController") as! AppTabBarController
        }
        
    }
    
    struct catalog {
        
        static private let storyboard = UIStoryboard(name: "Catalog", bundle: nil)
        
        static func catalogVC() -> CatalogViewController {
            return storyboard.instantiateViewController(withIdentifier: "CatalogViewController") as! CatalogViewController
        }
    }
    
    struct detail {
        static private let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        
        static func detailVC() -> MovieDetailViewController {
            return storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        }
        
    }
    
    struct favorites {
        
        static private let storyboard = UIStoryboard(name: "Favorites", bundle: nil)
        
        static func favoritesVC() -> FavoritesViewController {
            return storyboard.instantiateViewController(withIdentifier: "FavoritesViewController") as! FavoritesViewController
        }
        
    }
    
    struct imageZoom {
        
        static private let storyboard = UIStoryboard(name: "ImageZoom", bundle: nil)
        
        static func imageZoomVC() -> ImageZoomViewController {
            return storyboard.instantiateViewController(withIdentifier: "ImageZoomViewController") as! ImageZoomViewController
            
        }
        
    }
    
}

