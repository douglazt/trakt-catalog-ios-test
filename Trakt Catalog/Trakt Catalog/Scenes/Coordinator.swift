//
//  Coordinator.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import Foundation

protocol Coordinator {
    
    func start()
    
}

enum Coordinatorkey: String {
    case Catalog
    case MovieDetail
    case Favorites
    case App
}

