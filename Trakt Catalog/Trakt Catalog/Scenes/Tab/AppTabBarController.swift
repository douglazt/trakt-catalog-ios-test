//
//  AppTabBarController.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

class AppTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = .white
        self.tabBar.barTintColor = .black
    }
    
}
