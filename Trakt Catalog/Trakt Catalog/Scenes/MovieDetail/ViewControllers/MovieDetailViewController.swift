//
//  MovieDetailViewController.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieDetailViewController: BaseViewController {
    
    let tableViewRowEstimatedHeight: CGFloat = 180
    let tableViewGalleryCellheight: CGFloat = 180
    let iphone5Size: CGFloat = 568
    let galleryCellHeightForIphone5orLess: CGFloat = 150
    
    @IBOutlet weak var tableHeaderViewImage: UIImageView!
    @IBOutlet weak var customTableHeaderView: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var viewModel: MovieDetailViewModelType! {
        didSet{
            viewModel.viewDelegate = self
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParentViewController {
            viewModel.coordinatorDelegate?.didCloseDetailView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.start()
        setup()
    }
    
    func setup(){
        self.title = viewModel.selectedMovie.title
        tableview.dataSource = self
        tableview.delegate = self
        if UIDevice.current.userInterfaceIdiom == .pad  { // isIpad
            self.customTableHeaderView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 350)
        }
    }
    
    fileprivate func downloadBackgroundImage(from movieImages:[MovieImageType]?) {
        
        guard let imageUrl = movieImages?.first?.url else { return }
        guard let finalURL = URL(string: imageUrl) else { return }
        tableHeaderViewImage.af_setImage(withURL: finalURL) // download and cache image

    }
    
    //Loading
    private func setLoading(_ loading: Bool) {
        if loading {
            indicator.startAnimating()
        } else {
            indicator.stopAnimating()
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.tableview.reloadData()
        }
    }
    
}

extension MovieDetailViewController: MovieDetailViewModelViewDelegate {
    
    func updateScreen() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.tableview.reloadData()
        }
    }
    
    func updateState(state: DetailViewState) {
        DispatchQueue.main.async { [weak self] in
            
            guard let strongSelf = self else { return }
     
            switch state {
            case .loading:
                strongSelf.setLoading(true)
                break
            case .ready:
                strongSelf.setLoading(false)
                if let model = strongSelf.viewModel.movieImages {
                    strongSelf.downloadBackgroundImage(from: model.moviebackground)
                }
            default:
                break
            }
        }
    }
    
}

extension MovieDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == DetailTableViewSection.detailInfoSection.rawValue {
            guard let cell = tableview.dequeueReusableCell(withIdentifier: "MovieDetailTableViewCell") as? MovieDetailTableViewCell else { fatalError("Cell Identifier (MovieDetailTableViewCell) not found") }
            
            cell.configureCell(withMovie: viewModel.selectedMovie, andCompleteInfo: viewModel.completeMovieInfo)
            return cell
        } else {
            guard let cell = tableview.dequeueReusableCell(withIdentifier: "GallleryTableViewCell") as? GallleryTableViewCell else { fatalError("Cell Identifier (GallleryTableViewCell) not found") }
            
            cell.configureWithImagesTypesArray(viewModel.movieImages?.movieposter)
            cell.delegate = self
            return cell
        }
    }
    
}

extension MovieDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == DetailTableViewSection.carrouselSection.rawValue {
            return view.frame.maxY <= iphone5Size ? galleryCellHeightForIphone5orLess : tableViewGalleryCellheight
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewRowEstimatedHeight
    }
    
    
    
}

extension MovieDetailViewController: GalleryTableViewCellDelegate {
    func showSelectedImageInZoomView(galleryTableViewCell: GallleryTableViewCell, image: UIImage) {
        viewModel.coordinatorDelegate?.didTapImage(image: image, fromController: self)
    }
}
