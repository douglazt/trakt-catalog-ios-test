//
//  MovieDetailCoordinator.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

protocol DetailCoordinatorDelegate: class {
    func detailCoordinatorDidClose()
}

class MovieDetailCoordinator: Coordinator {
    
    weak var delegate: DetailCoordinatorDelegate?
    var window: UIWindow
    weak var rootController: UIViewController?
    var detailViewController: MovieDetailViewController?
    var movieSelected: Movie
    var imageZoomCoordinator: ImageZoomCoordinator?
    
    lazy var viewModel: MovieDetailViewModel = {
        let viewModel = MovieDetailViewModel(movie: movieSelected, service: serviceAPI, imageService: imageServiceAPI)
        viewModel.coordinatorDelegate = self
        return viewModel
    }()
    
    lazy var serviceAPI: ServiceAPI = {
        let apiService = APIClient()
        return apiService
    }()
    
    lazy var imageServiceAPI: ImageServiceAPI = {
       let imageService = ImageServicesAPIClient()
        return imageService
    }()
    
    init(window:UIWindow, rootController: UIViewController?, movie: Movie) {
        self.window = window
        self.movieSelected = movie
        self.rootController = rootController
    }
    
    func start() {
        
        configureViewController()
        
        if rootController == nil {
            let nav = BaseNavigationController(rootViewController: detailViewController!)
            window.rootViewController = nav
            if !window.isKeyWindow {
                window.makeKeyAndVisible()
            }
        } else {
            guard let nav = rootController as? UINavigationController else { return }
            nav.pushViewController(detailViewController!, animated: true)
        }
        
    }
    
    private func configureViewController() {
        detailViewController = StoryboardViewControllers.detail.detailVC()
        
        guard let _ = detailViewController else { return }
        detailViewController?.viewModel = viewModel
    }
    
}

extension MovieDetailCoordinator: MovieDetailViewModelCoordinatorDelegate {
    
    func didCloseDetailView() {
        delegate?.detailCoordinatorDidClose()
    }
    
    func didTapImage(image: UIImage, fromController controller: UIViewController) {
        imageZoomCoordinator = ImageZoomCoordinator(window: window, rootController: detailViewController, image: image)
        imageZoomCoordinator?.delegate = self
        imageZoomCoordinator?.start()
    }
}

extension MovieDetailCoordinator: ImageZoomCoordinatorDelegate {
    func didExitImageZoom(imageZoomCoordinator: ImageZoomCoordinator) {
        self.imageZoomCoordinator = nil
    }
}
