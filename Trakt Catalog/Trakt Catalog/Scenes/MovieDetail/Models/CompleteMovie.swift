//
//  CompleteMovie.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 25/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import Foundation

struct CompleteMovie: Codable {
    
    let title: String
    let year: Int
    let ids: MovieIds
    let tagline: String
    let overview: String
    let released: String
    let runtime: Int
    let country: String?
    let updated_at: Date
    let trailer: String?
    let homepage: String?
    let rating: Double
    let votes: Int
    let comment_count: Int
    let language: String?
    let available_translations: [String]?
    let genres: [String]
    let certification: String?
    
    enum CodingKeys: String, CodingKey {
        case title
        case year
        case ids
        case tagline
        case overview
        case released
        case runtime
        case country
        case updated_at
        case trailer
        case homepage
        case rating
        case votes
        case comment_count
        case language
        case available_translations
        case genres
        case certification
    }
    
}
