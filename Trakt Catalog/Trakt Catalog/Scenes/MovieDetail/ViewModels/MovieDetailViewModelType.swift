//
//  MovieDetailViewModelType.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

enum DetailViewState {
    case backgroundImage
    case gallery
    case loading
    case ready
}

protocol MovieDetailViewModelType {
 
    var viewDelegate: MovieDetailViewModelViewDelegate? { get set }
    var coordinatorDelegate: MovieDetailViewModelCoordinatorDelegate? { get set }
    
    func numberOfItems(inSection section: Int) -> Int
    
    var completeMovieInfo: CompleteMovie? { get set }
    var selectedMovie: Movie { get set }
    var movieImages: MovieImages? { get set }
    
    func start()
    
    func loadCompleteMovieInfo()
    func loadMovieImages()
    
}

protocol MovieDetailViewModelViewDelegate: class {
    
    func updateScreen()
    func updateState(state: DetailViewState)
    
}

protocol MovieDetailViewModelCoordinatorDelegate: class {
    
    func didCloseDetailView()
    func didTapImage(image: UIImage, fromController controller: UIViewController)
    
}

