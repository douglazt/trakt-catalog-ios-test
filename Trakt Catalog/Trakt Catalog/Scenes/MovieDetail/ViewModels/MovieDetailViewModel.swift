//
//  MovieDetailViewModel.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

enum DetailTableViewSection: Int {
    case detailInfoSection = 0
    case carrouselSection = 1
}

class MovieDetailViewModel: MovieDetailViewModelType {
    
    weak var coordinatorDelegate: MovieDetailViewModelCoordinatorDelegate?
    weak var viewDelegate: MovieDetailViewModelViewDelegate?
    
    let apiService: ServiceAPI
    let imagesApiService: ImageServiceAPI
    var selectedMovie: Movie
    var completeMovieInfo: CompleteMovie?
    var movieImages: MovieImages?
    
    init(movie: Movie, service: ServiceAPI, imageService: ImageServiceAPI) {
        self.selectedMovie = movie
        self.apiService = service
        self.imagesApiService = imageService
    }
    
    func start() {
        loadCompleteMovieInfo()
        loadMovieImages()
    }
    
    func numberOfItems(inSection section: Int) -> Int {
        return 1
    }
    
    func loadCompleteMovieInfo() {
        
        viewDelegate?.updateState(state: .loading)
        apiService.getMovie(byMovieID: selectedMovie) { [weak self] (completeMovie, error) in
            guard let strongSelf = self else { return }
            
            strongSelf.completeMovieInfo = completeMovie
            strongSelf.viewDelegate?.updateState(state: .ready)
            strongSelf.viewDelegate?.updateScreen()
        }
    }
    
    func loadMovieImages() {
        viewDelegate?.updateState(state: .loading)
        imagesApiService.getImagesForMovie(movie: selectedMovie) { [weak self] (movieImgs, error) in
            
            guard let strongSelf = self else { return }
            
            strongSelf.movieImages = movieImgs
            strongSelf.viewDelegate?.updateState(state: .ready)
            strongSelf.viewDelegate?.updateScreen()
        }
    }
    
}

