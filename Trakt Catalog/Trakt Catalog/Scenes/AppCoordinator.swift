//
//  AppCoordinator.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    
    var coordinators: [Coordinatorkey: Coordinator] = [:]
    
    var tabBarViewController: AppTabBarController?
    var window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        
        tabBarViewController = StoryboardViewControllers.app.tabVC()
        
        guard let _ = tabBarViewController else { return }
        
        configureTabs()
        
        window.rootViewController = tabBarViewController
        window.makeKeyAndVisible()
    }
    
    private func configureTabs() {
        let catalogCoordinator = CatalogCoordinator(window: window, rootController: tabBarViewController)
        coordinators[.Catalog] = catalogCoordinator
        catalogCoordinator.start()
        
        let favoritesCoordinator = FavoritesCoordinator(window: window, rootController: tabBarViewController)
        coordinators[.Favorites] = favoritesCoordinator
        favoritesCoordinator.start()
        
        catalogCoordinator.catalogNavigationVC?.tabBarItem.image = UIImage(named: "movie_catalog_white")
        favoritesCoordinator.favoritesNavigationViewController?.tabBarItem.image = UIImage(named: "fav_folder_white")
        
        tabBarViewController?.viewControllers = [catalogCoordinator.catalogNavigationVC!, favoritesCoordinator.favoritesNavigationViewController!]
    }
    
}
