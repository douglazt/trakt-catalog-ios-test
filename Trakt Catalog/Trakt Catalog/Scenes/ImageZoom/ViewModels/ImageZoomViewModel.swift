//
//  ImageZoomViewModel.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 26/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

class ImageZoomViewModel: ImageZoomViewModelType {
    
    
    weak var viewDelegate: ImageZoomViewModelViewDelegate?
    weak var coordinatorDelegate: ImageZoomViewModelCoordinatorDelegate?
    
    var image: UIImage
    
    init(image: UIImage) {
        self.image = image
    }
    
    func start() { }
    
}
