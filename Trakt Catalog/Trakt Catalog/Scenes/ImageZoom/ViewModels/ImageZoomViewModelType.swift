//
//  ImageZoomViewModel.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 26/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

protocol ImageZoomViewModelType {
    
    var viewDelegate: ImageZoomViewModelViewDelegate? { get set }
    var coordinatorDelegate: ImageZoomViewModelCoordinatorDelegate? { get set }
    
    var image: UIImage { get set }
    
    func start()
    
}

protocol ImageZoomViewModelViewDelegate: class {
    
    func updateScreen()
    
}

protocol ImageZoomViewModelCoordinatorDelegate: class {
    
    func didCloseZoomView()
    
}
