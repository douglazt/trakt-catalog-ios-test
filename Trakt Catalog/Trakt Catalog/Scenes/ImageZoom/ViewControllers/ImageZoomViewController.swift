//
//  ImageZoomViewController.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 26/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

class ImageZoomViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var scroll: UIScrollView!
    
    var viewModel: ImageZoomViewModelType! {
        didSet{
            viewModel.viewDelegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        scroll.minimumZoomScale = 1.0
        scroll.maximumZoomScale = 5.0
        scroll.delegate = self
        self.image.image = viewModel.image
        self.image.layer.borderColor = UIColor.black.cgColor
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.coordinatorDelegate?.didCloseZoomView()
    }

}

extension ImageZoomViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return image
    }
    
}

extension ImageZoomViewController: ImageZoomViewModelViewDelegate {
    
    func updateScreen() { }
}
