//
//  ImageZoomCoordinator.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 26/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

protocol ImageZoomCoordinatorDelegate: class {
    
    func didExitImageZoom(imageZoomCoordinator: ImageZoomCoordinator)

}

class ImageZoomCoordinator: Coordinator {
    
    var window: UIWindow
    var imageZoomNavigationController: BaseNavigationController?
    var imageZoomViewController: ImageZoomViewController?
    weak var rootController: UIViewController?
    weak var delegate: ImageZoomCoordinatorDelegate?
    var image: UIImage
    
    init(window: UIWindow, rootController: UIViewController?, image: UIImage) {
        self.window = window
        self.rootController = rootController
        self.image = image
    }
    
    lazy var viewModel: ImageZoomViewModel = {
        let viewModel = ImageZoomViewModel(image: image)
        viewModel.coordinatorDelegate = self
        return viewModel
    }()
    
    func start() {
        
        configureViewController()
        
        if rootController == nil {
            let nav = BaseNavigationController(rootViewController: imageZoomViewController!)
            window.rootViewController = nav
            if !window.isKeyWindow {
                window.makeKeyAndVisible()
            }
        } else {
            guard let nav = rootController?.navigationController as? BaseNavigationController else { return }
            nav.pushViewController(imageZoomViewController!, animated: true)
        }
        
    }
    
    private func configureViewController() {
        imageZoomViewController = StoryboardViewControllers.imageZoom.imageZoomVC()

        guard let _ = imageZoomViewController else { return }
        imageZoomNavigationController = BaseNavigationController(rootViewController: imageZoomViewController!)
        imageZoomViewController?.viewModel = viewModel
    }
    
}

extension ImageZoomCoordinator: ImageZoomViewModelCoordinatorDelegate {
    
    func didCloseZoomView() {
        delegate?.didExitImageZoom(imageZoomCoordinator: self)
    }
}
