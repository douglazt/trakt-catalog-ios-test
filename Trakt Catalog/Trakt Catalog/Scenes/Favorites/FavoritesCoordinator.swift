//
//  FavoritesCoordinator.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

class FavoritesCoordinator: Coordinator {
    
    
    var window: UIWindow
    var favoritesNavigationViewController: BaseNavigationController?
    var favoritesViewController: FavoritesViewController?
    weak var rootController: UIViewController?
    
    init(window: UIWindow, rootController: UIViewController?) {
        self.window = window
        self.rootController = rootController
    }
    
    func start() {
        
        configureViewController()
        
        if rootController == nil {
            window.rootViewController = favoritesNavigationViewController
            if !window.isKeyWindow {
                window.makeKeyAndVisible()
            }
        }
        
    }
    
    private func configureViewController() {
        favoritesViewController = StoryboardViewControllers.favorites.favoritesVC()
        
        guard let _ = favoritesViewController else { return }
        favoritesNavigationViewController = BaseNavigationController(rootViewController: favoritesViewController!)
        
        // set viewModel
    }
    
}
