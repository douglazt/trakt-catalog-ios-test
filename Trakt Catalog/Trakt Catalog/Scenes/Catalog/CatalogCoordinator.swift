//
//  CatalogCoordinator.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

class CatalogCoordinator: Coordinator {
    
    var detailCoordinator: MovieDetailCoordinator?
    var catalogViewController: CatalogViewController?
    var catalogNavigationVC: BaseNavigationController?
    let window: UIWindow
    
    weak var rootController: UIViewController?

    lazy var viewModel: CatalogViewModelType = {
       let viewModel = CatalogViewModel(apiService: serviceAPI)
        viewModel.coordinatorDelegate = self
        return viewModel
    }()
    
    lazy var serviceAPI: ServiceAPI = {
        let apiService = APIClient()
        return apiService
    }()
    
    init(window: UIWindow, rootController: UIViewController?) {
        self.window = window
        self.rootController = rootController
    }
    
    func start() {
        configureViewController()
        
        // When this VC is called from another VC and not from UIWindow.rootViewController
        if rootController == nil {
            window.rootViewController = catalogNavigationVC
            if !window.isKeyWindow {
                window.makeKeyAndVisible()
            }
        }
    }
    
    private func configureViewController() {
        catalogViewController = StoryboardViewControllers.catalog.catalogVC()
        
        guard let _ = catalogViewController else { return }
        catalogNavigationVC = BaseNavigationController(rootViewController: catalogViewController!)
        
        catalogViewController?.viewModel = viewModel
    }
    
}

extension CatalogCoordinator: CatalogViewModelCoordinatorDelegate {
    
    func didSelectDetailForItem(item: Movie) {
        // Call Detail coordinator
        detailCoordinator = MovieDetailCoordinator(window:window, rootController: catalogNavigationVC, movie: item)
        detailCoordinator?.delegate = self
        detailCoordinator?.start()
    }

}

extension CatalogCoordinator: DetailCoordinatorDelegate {
    func detailCoordinatorDidClose() {
        detailCoordinator = nil
    }
}
