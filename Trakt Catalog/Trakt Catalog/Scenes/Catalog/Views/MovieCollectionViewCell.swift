//
//  MovieCollectionViewCell.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var releaseData: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    
    let imageCache = AutoPurgingImageCache()
    
    override func prepareForReuse() {
        self.movieImage.image = nil
        self.movieTitle.text = nil
        self.releaseData.text = nil
        super.prepareForReuse()
    }
    
    func configureCell(withMovie movie: Movie) {
        
        self.movieTitle.text = movie.title
        self.releaseData.text = movie.year != nil ? "\(movie.year!)" : "No date"
        downloadPosterImage(withMovie: movie)
    }
    
    private func downloadPosterImage(withMovie movie: Movie) {
        
        guard let _ = movie.ids.imdb else { return }
        let cachedImage = imageCache.image(withIdentifier: movie.ids.imdb!)
        DispatchQueue.main.async {
            self.movieImage.image = cachedImage
            return
        }
        
        ImageServicesAPIClient.downloadImagePoster(forMovie: movie) { [weak self] (image, error) in
            guard let posterImage = image else { return }
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                strongSelf.movieImage.image = posterImage
            }
        }
        
    }
    
}
