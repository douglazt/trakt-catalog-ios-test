//
//  CatalogViewController.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

class CatalogViewController: BaseViewController {

    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 20.0)
    fileprivate let portratItemsPerRow: CGFloat = 2
    fileprivate let landscapeItemsPerRow: CGFloat = 3
    fileprivate let portraitCellHeightDivisionFactor:CGFloat = 2.75
    fileprivate let LandscapeCellHeightDivisionFactor:CGFloat = 1.5
    fileprivate let ipadCellHeightDivisionFactor: CGFloat = 3.0
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var viewModel: CatalogViewModelType! {
        didSet{
            viewModel.viewDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        viewModel.start()
    }
    
    func setup(){
        configureSearchController()
        configureNavigationButtons()
        collection.dataSource = self
        collection.delegate = self
        collection.backgroundColor = UIColor(red: 24/255, green: 25/255, blue: 27/255, alpha: 1.0)
    }
    
    private func configureNavigationButtons() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(reloadPopularMovies))
    }
    
    @objc private func reloadPopularMovies() {
        viewModel.resetPopularMovies()
    }
    
    private func configureSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        searchController.searchBar.delegate = self
    }

    //Loading
    private func setLoading(_ loading: Bool) {
        if loading {
            indicator.startAnimating()
        } else {
            indicator.stopAnimating()
        }
    }
    
    //Search Methods
    
    func searchMovies(bySearchText searchText: String) {
        if isSearching() {
            viewModel.searchMovies(byText: searchText.lowercased())
        }
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    fileprivate func sizeForCell(inOrientation orientation: UIDeviceOrientation) -> CGSize {
        
        if orientation == .landscapeLeft || orientation == .landscapeRight {
            let paddingSpace = sectionInsets.left * (landscapeItemsPerRow + 1)
            let availableWidth = view.frame.width - paddingSpace
            let widthPerItem = availableWidth / landscapeItemsPerRow
            
            return CGSize(width: widthPerItem, height: view.frame.height / LandscapeCellHeightDivisionFactor)
        }
        
        if UIDevice.current.userInterfaceIdiom == .pad  { // isIpad
            let paddingSpace = sectionInsets.left * (landscapeItemsPerRow + 1)
            let availableWidth = view.frame.width - paddingSpace
            let widthPerItem = availableWidth / landscapeItemsPerRow
            
            return CGSize(width: widthPerItem, height: view.frame.height / ipadCellHeightDivisionFactor)
        }
        
        let paddingSpace = sectionInsets.left * (portratItemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / portratItemsPerRow
    
        return CGSize(width: widthPerItem, height: view.frame.height / portraitCellHeightDivisionFactor)
    }
    
}

extension CatalogViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        viewModel.isSearching = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.isSearching = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchMovies(bySearchText: searchBar.text!)
    }

}

extension CatalogViewController: UISearchBarDelegate { }

extension CatalogViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as? MovieCollectionViewCell else { fatalError("Cell type not found") }
        cell.configureCell(withMovie: viewModel.itemForItemIndex(index: indexPath.item))
        
        return cell
    }
    
}

extension CatalogViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.coordinatorDelegate?.didSelectDetailForItem(item: viewModel.itemForItemIndex(index: indexPath.item))
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastItem = viewModel.numberOfItems(inSection: indexPath.section) - 1
        if indexPath.item == lastItem {
            if !viewModel.isLoading && !viewModel.isSearching {
                viewModel.currentPage += 1
                viewModel.loadPopularMovies()
            }

        }
    }
    
}

extension CatalogViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        return sizeForCell(inOrientation: UIDevice.current.orientation)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }

    
}

extension CatalogViewController: CatalogViewModelViewDelegate {
    
    func updateScreen() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.collection.reloadData()
        }
    }
    
    func updateState(state: CatalogState) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            switch state {
            case .searching, .loading:
                strongSelf.viewModel.isLoading = true
                strongSelf.setLoading(true)
            case .ready:
                strongSelf.viewModel.isLoading = false
                strongSelf.setLoading(false)
            }
        }
    }
    
}


