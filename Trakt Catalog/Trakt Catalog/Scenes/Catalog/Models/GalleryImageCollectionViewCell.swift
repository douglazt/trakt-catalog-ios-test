//
//  GalleryImageCollectionViewCell.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 25/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit
import AlamofireImage

protocol GallleryImageCollectionViewCellDelegate: class {
    func didTapCell(galleryImageCollectionViewCell: GalleryImageCollectionViewCell, forImage: UIImage)
}

class GalleryImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var posterImage: UIImageView!
    weak var delegate: GallleryImageCollectionViewCellDelegate?
    
    func configImage(withURL url: String?) {
        downloadPosterImage(withImageURL: url)
    }
    
    override func prepareForReuse() {
        posterImage.image = nil
        super.prepareForReuse()
    }
    
    override var isSelected: Bool {
        didSet{
            if self.isSelected {
                if let image = posterImage.image {
                    delegate?.didTapCell(galleryImageCollectionViewCell: self, forImage: image)
                }
            }
        }
    }
    
    private func downloadPosterImage(withImageURL url: String?) {
        
        guard let imageUrl = url else { return }
        guard let finalURL = URL(string: imageUrl) else { return }
        //let filter = AspectScaledToFillSizeFilter(size: posterImage.frame.size)
        posterImage.af_setImage(withURL: finalURL) // download and cache image
    }
    
}
