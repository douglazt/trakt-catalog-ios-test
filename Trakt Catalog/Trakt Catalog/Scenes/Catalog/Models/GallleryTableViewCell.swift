//
//  GallleryTableViewCell.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 25/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

protocol GalleryTableViewCellDelegate: class {
    func showSelectedImageInZoomView(galleryTableViewCell: GallleryTableViewCell, image: UIImage)
}

class GallleryTableViewCell: UITableViewCell {

    fileprivate let sectionInsets = UIEdgeInsets(top: 5.0, left: 8.0, bottom: 5.0, right: 8.0)
//    fileprivate let itemsPerRow: CGFloat = 3
//    fileprivate let itemHeight: CGFloat = 140
    
    //fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 20.0)
    fileprivate let portratItemsPerRow: CGFloat = 3
    fileprivate let landscapeItemsPerRow: CGFloat = 4
    fileprivate let portraitCellHeightDivisionFactor:CGFloat = 2.75
    fileprivate let LandscapeCellHeightDivisionFactor:CGFloat = 1.25
    
    weak var delegate: GalleryTableViewCellDelegate?
    
    @IBOutlet weak var imagesCollection: UICollectionView!
    var imageTypesArray: [MovieImageType]?
    
    override func prepareForReuse() {
        imageTypesArray = []
        super.prepareForReuse()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imagesCollection.dataSource = self
        imagesCollection.delegate = self
    }
    
    func configureWithImagesTypesArray(_ images: [MovieImageType]?) {
        imageTypesArray = images
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.imagesCollection.reloadData()
        }
    }

    fileprivate func sizeForCell(inOrientation orientation: UIDeviceOrientation) -> CGSize {
        
        if orientation == .landscapeLeft || orientation == .landscapeRight {
            let paddingSpace = sectionInsets.left * (landscapeItemsPerRow + 1)
            let availableWidth = frame.width - paddingSpace
            let widthPerItem = availableWidth / landscapeItemsPerRow
            
            return CGSize(width: widthPerItem, height: frame.height / LandscapeCellHeightDivisionFactor)
        }
        
        let paddingSpace = sectionInsets.left * (portratItemsPerRow + 1)
        let availableWidth = frame.width - paddingSpace
        let widthPerItem = availableWidth / portratItemsPerRow
        
        return CGSize(width: widthPerItem, height: frame.height)
    }
    
}

extension GallleryTableViewCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = imageTypesArray?.count {
            return count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryImageCollectionViewCell", for: indexPath) as? GalleryImageCollectionViewCell else { fatalError("cell (GalleryImageCollectionViewCell) not found") }
        
        cell.configImage(withURL: imageTypesArray?[indexPath.item].url)
        cell.delegate = self
        return cell
    }
    
}

extension GallleryTableViewCell: UICollectionViewDelegate {
    
}

extension GallleryTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return sizeForCell(inOrientation: UIDevice.current.orientation)
//        let paddingSpace = sectionInsets.left
//        let availableWidth = frame.width - paddingSpace
//        let widthPerItem = availableWidth / itemsPerRow
//
//        return CGSize(width: widthPerItem, height: itemHeight)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    
}

extension GallleryTableViewCell: GallleryImageCollectionViewCellDelegate {
    
    func didTapCell(galleryImageCollectionViewCell: GalleryImageCollectionViewCell, forImage: UIImage) {
        if !imagesCollection.isDragging {
            delegate?.showSelectedImageInZoomView(galleryTableViewCell: self, image: forImage)
        }
    }
}

