//
//  MovieDetailsTableViewCell.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 25/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

class MovieDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var taglineLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var releasedLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    func configureCell(withMovie movie: Movie, andCompleteInfo info: CompleteMovie?) {
        
        self.movieNameLabel.text = movie.title
        if let movieInfo = info {
            self.taglineLabel.text = movieInfo.tagline
            self.runtimeLabel.text = "\(movieInfo.runtime) minutes"
            self.ratingLabel.text = String(format: "%.2f", movieInfo.rating)
            self.genresLabel.text = movieInfo.genres.joined(separator: ",")
            self.releasedLabel.text = movieInfo.released
            self.overviewLabel.text = movieInfo.overview
        } else {
            self.taglineLabel.text = ""
            self.runtimeLabel.text = ""
            self.ratingLabel.text = ""
            self.genresLabel.text = ""
            self.releasedLabel.text = ""
            self.overviewLabel.text = ""
        }
        
        
    }
    
}

