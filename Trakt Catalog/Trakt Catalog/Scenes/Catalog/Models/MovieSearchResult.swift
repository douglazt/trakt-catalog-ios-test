//
//  MovieSearchResults.swift
//  Trakt Catalog
//
//  Created by Douglas da silva santos on 25/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import Foundation

struct MovieSearchResult: Codable {
    
    let type: String
    let score: Double
    let movie: Movie
    
}

