//
//  MovieImages.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import Foundation

struct MovieImages: Codable {
    
    let movieName: String
    let tmdb_id: String
    let imdb_id: String
    let hdmovielogo: [MovieImageType]
    let moviedisc: [MovieDisc]
    let movielogo: [MovieImageType]?
    let movieposter: [MovieImageType]
    let hdmovieclearart: [MovieImageType]
    let moviebackground: [MovieImageType]
    let moviebanner: [MovieImageType]
    let moviethumb: [MovieImageType]
    
    enum CodingKeys: String, CodingKey {
        case movieName = "name"
        case tmdb_id
        case imdb_id
        case hdmovielogo
        case moviedisc
        case movielogo
        case movieposter
        case hdmovieclearart
        case moviebackground
        case moviebanner
        case moviethumb
    }
    
}

struct MovieImageType: Codable {
    let id: String
    let url: String
    let lang: String
    let likes: String
}

struct MovieDisc: Codable {
    
    let id: String
    let url: String
    let lang: String
    let likes: String
    let disc: String
    let disc_type: String
    
}
