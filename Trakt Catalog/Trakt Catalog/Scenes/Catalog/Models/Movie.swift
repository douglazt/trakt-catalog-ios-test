//
//  Movie.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import Foundation

struct Movie: Codable {
    
    let title: String
    let year: Int?
    let ids: MovieIds
    
    enum CodingKeys: String, CodingKey {
        
        case title = "title"
        case year = "year"
        case ids = "ids"
    }
    
}

struct MovieIds: Codable {
    let trakt: Int
    let slug: String
    let imdb: String?
    let tmdb: Int
    
    enum CodingKeys: String, CodingKey {
        case trakt = "trakt"
        case slug = "slug"
        case imdb = "imdb"
        case tmdb = "tmdb"
    }
    
}
