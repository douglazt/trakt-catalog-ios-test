//
//  CatalogViewModel.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import Foundation

class CatalogViewModel: CatalogViewModelType {
    
    weak var viewDelegate: CatalogViewModelViewDelegate?
    weak var coordinatorDelegate: CatalogViewModelCoordinatorDelegate?
    
    var popularMovies: [Movie] = []
    var popularMoviesFromSearch: [Movie] = []
    let apiService: ServiceAPI
    var isSearching: Bool = false {
        didSet(newValue){
            if !newValue {
                popularMoviesFromSearch.removeAll()
            }
            viewDelegate?.updateScreen()
        }
    }
    var currentPage: Int = 1
    var isLoading: Bool = false
    
    lazy var imageServiceAPI: ImageServiceAPI = {
       let imageService = ImageServicesAPIClient()
        return imageService
    }()
    
    init(apiService: ServiceAPI) {
        self.apiService = apiService
    }
    
    func numberOfItems(inSection section: Int) -> Int {
        return isSearching ? popularMoviesFromSearch.count : popularMovies.count
    }
    
    func itemForItemIndex(index: Int) -> Movie {
        return isSearching ? popularMoviesFromSearch[index] : popularMovies[index]
    }
    
    func start() {
        loadPopularMovies()
    }
    
    func resetPopularMovies() {
        currentPage = 1
        popularMovies = []
        viewDelegate?.updateScreen()
        loadPopularMovies()
    }
    
    func loadPopularMovies() {
        viewDelegate?.updateState(state: .loading)
        apiService.getPopularMovies(atPage: currentPage) {[weak self] (movies, error) in
            guard let strongSelf = self else { return }
            
            if error != nil {
                strongSelf.viewDelegate?.updateState(state: .ready)
                strongSelf.viewDelegate?.updateScreen()
                return
            }
            
            if let returnedMovies = movies {
                strongSelf.addMoviesToPopularMovies(returnedMovies)
                strongSelf.viewDelegate?.updateState(state: .ready)
                strongSelf.viewDelegate?.updateScreen()
                return
            }
        }
    }
    
    private func addMoviesToPopularMovies(_ list:[Movie]) {
        if list.count == 0 {
            return
        }
        
        for movie in list {
            let contains = popularMovies.contains(where: { (savedMovie) -> Bool in
                return movie.ids.trakt == savedMovie.ids.trakt
            })
            
            if !contains {
                popularMovies.append(movie)
            }
        }
    }
    
    func searchMovies(byText text: String) {
        viewDelegate?.updateState(state: .searching)
        apiService.getMoviesBySearchText(searchText: text) { [weak self] (searchResults, error) in
            guard let strongSelf = self else { return }
            
            if error != nil {
                strongSelf.viewDelegate?.updateState(state: .ready)
                strongSelf.viewDelegate?.updateScreen()
                return
            }
            
            if let results = searchResults {
                strongSelf.popularMoviesFromSearch = results.map({ (searchRes) in
                    return searchRes.movie
                })
                strongSelf.viewDelegate?.updateState(state: .ready)
                strongSelf.viewDelegate?.updateScreen()
                return
            }
            
        }
        
    }
    
}
