//
//  CatalogViewModelType.swift
//  Trakt Catalog
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit

enum CatalogState {
    case loading
    case searching
    case ready
}

protocol CatalogViewModelType {
    
    var viewDelegate: CatalogViewModelViewDelegate? { get set }
    var coordinatorDelegate: CatalogViewModelCoordinatorDelegate? { get set }
    
    func numberOfItems(inSection section: Int) -> Int
    func itemForItemIndex(index: Int) -> Movie
    var isSearching: Bool { get set }
    var currentPage: Int { get set }
    var isLoading: Bool { get set }
    
    func start()
    
    func loadPopularMovies()
    func resetPopularMovies()
    func searchMovies(byText text: String)
    
}

protocol CatalogViewModelViewDelegate: class {
    
    func updateScreen()
    
    func updateState(state: CatalogState)
    
}

protocol CatalogViewModelCoordinatorDelegate: class {
    
    func didSelectDetailForItem(item: Movie)
    
}









