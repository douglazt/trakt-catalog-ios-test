//
//  MovieDetailViewModelTests.swift
//  Trakt CatalogTests
//
//  Created by Douglas da silva santos on 25/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import XCTest
@testable import Trakt_Catalog

class MovieDetailViewModelTests: XCTestCase {
    
    var movieDetailViewModel: MovieDetailViewModel!
    var selectedMovie: Movie!
    
    override func setUp() {
        super.setUp()
        selectedMovie = Movie(title: "TRON: Legacy", year: 2010, ids: MovieIds(trakt: 12601, slug: "tron-legacy-2010", imdb: "tt1104001", tmdb: 20526))
        movieDetailViewModel = MovieDetailViewModel(movie: selectedMovie, service: MockedSuccessServiceAPI(), imageService: MockedSuccessImageServiceAPI())
    }
    
    override func tearDown() {
        super.tearDown()
        selectedMovie = nil
        movieDetailViewModel = nil
    }
    
    func testThatCanLoadMovieCompleteInfo() {
        
        movieDetailViewModel.loadCompleteMovieInfo()
        XCTAssertNotNil(movieDetailViewModel.completeMovieInfo)
    }
    
    func testThatCanLoadListOfImagesForMovie() {
        
        movieDetailViewModel.loadMovieImages()
        XCTAssertNotNil(movieDetailViewModel.movieImages)
        
    }
    
    func testThatCanLoadBackgroundImageForMovie() {
        let viewDelegate = MockedMovieDetailViewModelViewDelegate()
        movieDetailViewModel.viewDelegate = viewDelegate
        movieDetailViewModel.loadMovieImages()
        
        XCTAssertTrue(viewDelegate.hasSetBackgroundImage)
    }
    
}


