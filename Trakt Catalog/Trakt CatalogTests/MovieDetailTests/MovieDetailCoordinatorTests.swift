//
//  MovieDetailCoordinator.swift
//  Trakt CatalogTests
//
//  Created by Douglas da silva santos on 25/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import XCTest
@testable import Trakt_Catalog

class MovieDetailCoordinatorTests: XCTestCase {
    
    var movieDetailCoordinator: MovieDetailCoordinator!
    var window: UIWindow!
    var rootController: UIViewController?
    var selectedMovie: Movie!
    
    override func setUp() {
        super.setUp()
        window = UIWindow(frame: UIScreen.main.bounds)
        selectedMovie = Movie(title: "TRON: Legacy", year: 2010, ids: MovieIds(trakt: 12601, slug: "tron-legacy-2010", imdb: "tt1104001", tmdb: 20526))
        movieDetailCoordinator = MovieDetailCoordinator(window: window, rootController: nil, movie: selectedMovie)
        
    }
    
    override func tearDown() {
        super.tearDown()
        
        window = nil
        selectedMovie = nil
        movieDetailCoordinator = nil
        
    }
    
    func testThatCanStartAsRootController() {
        
        movieDetailCoordinator.start()
        let nav = window.rootViewController as? BaseNavigationController
        let first = nav?.viewControllers.first as? MovieDetailViewController
        XCTAssertNotNil(first)
        
    }
    
}
