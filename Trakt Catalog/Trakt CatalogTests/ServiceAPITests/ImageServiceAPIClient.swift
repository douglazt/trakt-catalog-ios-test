//
//  ImageServiceAPIClient.swift
//  Trakt CatalogTests
//
//  Created by Douglas da Silva on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import XCTest
@testable import Trakt_Catalog

class ImageServiceAPIClient: XCTestCase {
    
    var imageApiClient: ImageServicesAPIClient!
    
    override func setUp() {
        super.setUp()
        imageApiClient = ImageServicesAPIClient()
    }
    
    override func tearDown() {
        super.tearDown()
        imageApiClient = nil
    }
    
    func test_that_can_get_imagesResponse_for_Movie() {

        //given
        let movie = Movie(title: "The Lord of the Rings: The Fellowship of the Ring", year: 2000, ids: MovieIds(trakt: 0, slug: "12", imdb: "tt0120737", tmdb: 120))

        let expect = self.expectation(description: "Expect for movie Images list")

        //when
        imageApiClient.getImagesForMovie(movie: movie) { (movieImages, error) in
            if error == nil {
                expect.fulfill()
            } else {
                XCTFail("Could not retrieve images")
            }
        }

        //then
        self.wait(for: [expect], timeout: 5.0)

    }
    
    func test_that_receives_error_with_invalid_movieID() {
        //given
        let movie = Movie(title: "The Lord of the Rings: The Fellowship of the Ring", year: 2000, ids: MovieIds(trakt: 0, slug: "12", imdb: "xxxxxxxx", tmdb: 000000))
        
        let expect = self.expectation(description: "Expect for movie Images list")
        
        //when
        imageApiClient.getImagesForMovie(movie: movie) { (movieImages, error) in
            if error != nil {
                expect.fulfill()
            } else {
                XCTFail("Should get Error")
            }
        }
        
        //then
        self.wait(for: [expect], timeout: 3.0)
    }
    
    
    func test_that_can_download_image_by_url() {
        //given
        let url = "http://assets.fanart.tv/fanart/movies/120/moviebanner/the-lord-of-the-rings-the-fellowship-of-the-ring-520e7bf6818ef.jpg"
        let expect = self.expectation(description: "Image download")
        
        //when
        ImageServicesAPIClient.downloadImage(fromURL: url) { (data, error) in
            if error == nil {
                expect.fulfill()
            } else {
                XCTFail("Download failed")
            }

        }
        
        //then
        self.wait(for: [expect], timeout: 3.0)
        
    }
        
}

