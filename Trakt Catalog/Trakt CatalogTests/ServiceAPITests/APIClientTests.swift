//
//  APIClientTests.swift
//  Trakt CatalogTests
//
//  Created by Douglas da silva santos on 24/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import XCTest
@testable import Trakt_Catalog

class APIClientTests: XCTestCase {
    
    var apiClient: APIClient!
    
    override func setUp() {
        super.setUp()
        apiClient = APIClient()
    }
    
    override func tearDown() {
        super.tearDown()
        apiClient = nil
    }
    
    func test_that_can_get_popular_movies() {
        //given
        let page = 1
        let expect = self.expectation(description: "Popular movies")
        
        //when
        apiClient.getPopularMovies(atPage: page) { (response, error) in
            if error == nil && response?.count == 10 {
                expect.fulfill()
            } else {
                XCTFail("Movies API Error")
            }
        }
        
        //then
        self.wait(for: [expect], timeout: 3.0)
        
    }

    func test_that_receives_error_with_invalid_url() {
        //given
        let page = 1
        let expect = self.expectation(description: "Popular movies")
        apiClient.baseURL = "https://api.trakt.tv.error.url"
        //when
        apiClient.getPopularMovies(atPage: page) { (response, error) in
            if error != nil {
                expect.fulfill()
            } else {
                XCTFail("Should get Error")
            }
        }
        
        //then
        self.wait(for: [expect], timeout: 3.0)
    }
    
    func testThatCanGetMovieByMovieID() {
        //given
        let movie = Movie(title: "asdsa", year: 2018, ids: MovieIds(trakt: 12601, slug: "tron-legacy-2010", imdb: "tt1104001", tmdb: 20526))
        let expect = self.expectation(description: "Movie Details")
        
        //when
        apiClient.getMovie(byMovieID: movie) { (completeMovie, error) in
            if error == nil && completeMovie?.title == "TRON: Legacy" {
                expect.fulfill()
            } else {
                XCTFail("Movies API Error")
            }
        }
        
        //then
        self.wait(for: [expect], timeout: 3.0)

    }
    
    func testThatCanGetMoviesBySearchText() {
        //given
        let text = "tron"
        let expect = self.expectation(description: "Movie Search")
        
        //when
        apiClient.getMoviesBySearchText(searchText: text) { (searchResults, error) in
            
            if let results = searchResults {
                if error == nil && results.count > 0 {
                    expect.fulfill()
                }
            } else {
                XCTFail("Movies API Error")
            }
        }
        
        //then
        self.wait(for: [expect], timeout: 3.0)

    }
    
    
}
