//
//  CatalogViewModelTests.swift
//  Trakt CatalogTests
//
//  Created by Douglas da silva santos on 25/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import XCTest
@testable import Trakt_Catalog

class CatalogViewModelTests: XCTestCase {
    
    var succesCatalogViewModel: CatalogViewModel!
    var failureCatalogViewModel: CatalogViewModel!
    
    override func setUp() {
        super.setUp()
        
        succesCatalogViewModel = CatalogViewModel(apiService: MockedSuccessServiceAPI())
        failureCatalogViewModel = CatalogViewModel(apiService: MockedFailureServiceAPI())
    }
    
    override func tearDown() {
        super.tearDown()
        succesCatalogViewModel = nil
        failureCatalogViewModel = nil
    }
    
    func testThatViewModelCanLoadPopularMovies() {
        
        succesCatalogViewModel.loadPopularMovies()
        XCTAssertEqual(succesCatalogViewModel.popularMovies.count, 1)
    }
    
    func testThatViewModelReceivesErrorWhenLoadingPopularMovies() {
        
        failureCatalogViewModel.loadPopularMovies()
        XCTAssertEqual(failureCatalogViewModel.popularMovies.count, 0)
    }
    
    func testThatViewModelDoesNotNeedToCallService() {
        let movie = Movie(title: "Test Movie", year: 2018, ids: MovieIds(trakt: 0, slug: "", imdb: "", tmdb: 0))
        let viewDelegate = MockedCatalogViewModelViewDelegate()
        succesCatalogViewModel.viewDelegate = viewDelegate
        succesCatalogViewModel.popularMovies = [movie]
        succesCatalogViewModel.loadPopularMovies()
        
        XCTAssertTrue(viewDelegate.didUpdateScreen)
    }
    
}
