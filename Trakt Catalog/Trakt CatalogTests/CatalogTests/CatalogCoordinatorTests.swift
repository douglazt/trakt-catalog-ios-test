//
//  CatalogCoordinator.swift
//  Trakt CatalogTests
//
//  Created by Douglas da silva santos on 25/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import XCTest
@testable import Trakt_Catalog

class CatalogCoordinatorTests: XCTestCase {

    var catalogCoordinator: CatalogCoordinator!
    var window: UIWindow!
    var rootController: UIViewController?
    
    override func setUp() {
        super.setUp()
        window = UIWindow(frame: UIScreen.main.bounds)
        catalogCoordinator = CatalogCoordinator(window: window, rootController: nil)
        
    }
    
    override func tearDown() {
        super.tearDown()
        window = nil
        catalogCoordinator = nil
    }

    func testThatCanConfigurePropertiesOnStart() {
        
        catalogCoordinator.start()
        
        XCTAssertNotNil(catalogCoordinator.catalogViewController)
        XCTAssertNotNil(catalogCoordinator.viewModel)
        XCTAssertNotNil(catalogCoordinator.catalogNavigationVC)
        
    }
    
    func testThatCanOpenDetailItemFromCatalog() {
        
        catalogCoordinator.start()
        catalogCoordinator.viewModel = MockedCatalogViewModelType()
        catalogCoordinator.didSelectDetailForItem(item: catalogCoordinator.viewModel.itemForItemIndex(index: 0))
        
        XCTAssertNotNil(catalogCoordinator.detailCoordinator)
        
    }
    
}

