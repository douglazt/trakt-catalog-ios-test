//
//  MockedClasses.swift
//  Trakt CatalogTests
//
//  Created by Douglas da silva santos on 25/07/2018.
//  Copyright © 2018 Douglas. All rights reserved.
//

import UIKit
@testable import Trakt_Catalog

class MockedCatalogViewModelViewDelegate: CatalogViewModelViewDelegate {
    
    var didUpdateScreen = false
    
    func updateScreen() {
        didUpdateScreen = true
    }
    func updateState(state: CatalogState) { }
    
}

class MockedSuccessServiceAPI: ServiceAPI {
    
    var clientID: String = ""
    var APIVersion: String = ""
    var baseURL: String = ""
    var headers: [String : String] = ["": ""]
    
    func getPopularMovies(atPage page: Int, completion: @escaping ServiceAPI.popularMoviesResponse) {
        let movie = Movie(title: "Test movie", year: 2018, ids: MovieIds(trakt: 0, slug: "", imdb: "", tmdb: 0))
        completion([movie], nil)
    }
    
    func getMovie(byMovieID movie: Movie, completion: @escaping ServiceAPI.movieDetailsResponse) {
        let completeMovie = CompleteMovie(title: "Test movie", year: 2018, ids: MovieIds(trakt: 0, slug: "", imdb: "", tmdb: 0), tagline: "", overview: "", released: "", runtime: 100, country: "", updated_at: Date(), trailer: "", homepage: "", rating: 8.5, votes: 100, comment_count: 100, language: "", available_translations: ["en"], genres: ["action", "comedy"], certification: "")
        completion(completeMovie, nil)
    }
    
    func parseResponseData<T>(responseData data: Data, forResultType type: T.Type) -> T? where T : Decodable, T : Encodable {
        return try? JSONDecoder().decode(type, from: data)
    }
    
    func getMoviesBySearchText(searchText text: String, completion: @escaping ServiceAPI.moviesSearchResponse) { }
}

class MockedFailureServiceAPI: ServiceAPI {
    
    var clientID: String = ""
    var APIVersion: String = ""
    var baseURL: String = ""
    var headers: [String : String] = ["": ""]
    
    func getPopularMovies(atPage page: Int, completion: @escaping ServiceAPI.popularMoviesResponse) {
        completion(nil, NSError(domain: "TraktAPI", code: 999, userInfo: nil))
    }
    
    func getMovie(byMovieID movie: Movie, completion: @escaping ServiceAPI.movieDetailsResponse) { }
    
    func parseResponseData<T>(responseData data: Data, forResultType type: T.Type) -> T? where T : Decodable, T : Encodable {
        return try? JSONDecoder().decode(type, from: Data())
    }
    
    func getMoviesBySearchText(searchText text: String, completion: @escaping ServiceAPI.moviesSearchResponse) { }
}

class MockedCatalogViewModelType: CatalogViewModelType {
    
    var isSearching: Bool = false
    var currentPage: Int = 1
    var isLoading: Bool = false
    
    func resetPopularMovies() { }
    
    var viewDelegate: CatalogViewModelViewDelegate?
    var coordinatorDelegate: CatalogViewModelCoordinatorDelegate?
    func numberOfItems(inSection section: Int) -> Int { return 1 }
    
    func itemForItemIndex(index: Int) -> Movie {
        return Movie(title: "Test Movie", year: 2018, ids: MovieIds(trakt: 0, slug: "", imdb: "", tmdb: 0))
    }
    
    func start() { }
    func loadPopularMovies() { }
    func searchMovies(byText text: String) { }
    
}

class MockedSuccessImageServiceAPI: ImageServiceAPI {
    var apiKey: String = ""
    var baseURL: String = ""
    var headers: [String : String] = ["": ""]
    
    func getImagesForMovie(movie: Movie, completion: @escaping ImageServiceAPI.ImagesResponse) {
        let movieImageTypeArray = [MovieImageType(id: "", url: "", lang: "", likes: "")]
        let movieImages = MovieImages(movieName: "", tmdb_id: "", imdb_id: "", hdmovielogo: movieImageTypeArray, moviedisc: [MovieDisc(id: "", url: "", lang: "", likes: "", disc: "", disc_type: "")], movielogo: movieImageTypeArray, movieposter: movieImageTypeArray, hdmovieclearart: movieImageTypeArray, moviebackground: movieImageTypeArray, moviebanner: movieImageTypeArray, moviethumb: movieImageTypeArray)
        
        completion(movieImages, nil)
    }
    
    static func downloadImage(fromURL url: String, completion: @escaping ImageServiceAPI.ImageDownloadCompletion) { }
    static func downloadImagePoster(forMovie movie: Movie, completion: @escaping ImageServiceAPI.ImageDownloadCompletion) { }
    
    func parseResponseData<T>(responseData data: Data, forResultType type: T.Type) -> T? where T : Decodable, T : Encodable {
        return try? JSONDecoder().decode(type, from: data)
    }
    
}

class MockedDetailViewModel: MovieDetailViewModelType {
    
    var viewDelegate: MovieDetailViewModelViewDelegate?
    var coordinatorDelegate: MovieDetailViewModelCoordinatorDelegate?
    
    func numberOfItems(inSection section: Int) -> Int {
        return 1
    }
    
    var completeMovieInfo: CompleteMovie?
    var selectedMovie: Movie = Movie(title: "TRON: Legacy", year: 2010, ids: MovieIds(trakt: 12601, slug: "tron-legacy-2010", imdb: "tt1104001", tmdb: 20526))
    var movieImages: MovieImages?
    
    func start() {
        loadMovieImages()
    }
    
    func loadCompleteMovieInfo() {
        completeMovieInfo = CompleteMovie(title: "Test movie", year: 2018, ids: MovieIds(trakt: 0, slug: "", imdb: "", tmdb: 0), tagline: "", overview: "", released: "", runtime: 100, country: "", updated_at: Date(), trailer: "", homepage: "", rating: 8.5, votes: 100, comment_count: 100, language: "", available_translations: ["en"], genres: ["action", "comedy"], certification: "")
    }
    
    func loadMovieImages() {
        let movieImageTypeArray = [MovieImageType(id: "", url: "http://assets.fanart.tv/fanart/movies/120/moviebackground/the-lord-of-the-rings-the-fellowship-of-the-ring-4fdb8b38d6453.jpg", lang: "", likes: "")]
        
        let mockMovieImages = MovieImages(movieName: "", tmdb_id: "", imdb_id: "", hdmovielogo: movieImageTypeArray, moviedisc: [MovieDisc(id: "", url: "", lang: "", likes: "", disc: "", disc_type: "")], movielogo: movieImageTypeArray, movieposter: movieImageTypeArray, hdmovieclearart: movieImageTypeArray, moviebackground: movieImageTypeArray, moviebanner: movieImageTypeArray, moviethumb: movieImageTypeArray)
        movieImages = mockMovieImages
        
        viewDelegate?.updateState(state: .backgroundImage)
    }
    
}

class MockedMovieDetailViewModelViewDelegate: MovieDetailViewModelViewDelegate {
    
    var hasSetBackgroundImage: Bool = false
    var hasUpdateScreen = false
    
    func updateScreen() {
        hasUpdateScreen = true
    }
    
    func updateState(state: DetailViewState) {
        if state == .ready {
            hasSetBackgroundImage = true
        }
    }
    
    
}


