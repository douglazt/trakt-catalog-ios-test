# trakt-catalog-ios-test

iOS App for search the most popular movies in the trakt API.

# Usage

To Test the Example Application you will need to install carthage.

[carthage](https://github.com/Carthage/Carthage)

After carthage installation, inside the project directory, run `carthage bootstrap` to install the dependencies.

# Info

The project was developed in Xcode 9.4.1 and the dependency frameworks were built in the same version of Xcode.  
The application can run in ios 11.0+.  
The application is Universal.

# Architecture

The project was developed using the MVVM-C iOS Architecture.  
See more about MVVM-C [here](https://marcosantadev.com/mvvmc-with-swift/).  
The project uses only 2 third party libraries - `Alamofire` and `AlamofireImage`.  
[Almofire](https://github.com/Alamofire/Alamofire).

### Thank you

Some features are not complete due to the lack of time, but I did the best I could to create a good App.  
Hope you enjoy.



